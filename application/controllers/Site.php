<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	public function index()
	{
		$this->load->view('site/index');
	}

	#Login page
	public function login()
	{
		$this->load->view('site/login');
	}

	#Register Page
	public function register()
	{
		$this->load->view('site/register');
	}
	public function dashboard()
	{
		$this->load->view('web/header');
		$this->load->view('web/dashboard');
		$this->load->view('web/footer');
	}

}
