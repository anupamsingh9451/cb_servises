$(document).ready(function() {
    /* login mso */
    $("#loginlco").on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: base_loc + 'sitepostajax/lcologin',
            data: 'username=' + $('.username').val() + '&password=' + $('.password').val(),
            headers: {
                'Client-Service': clientserv,
                'Auth-Key': apikey,
            },
            beforeSend: function() {
                $('.loginlco').attr("disabled", "disabled");
                $('.loginlco').html("Please Wait...");
            },
            success: function(msg) {
                if (msg.message == "200") {
                    $('.mymsg').html("Login Successfull");
                    $('.mymsg').removeClass('alert-warning');
                    $('.mymsg').addClass('alert-success');
                    location.href = msg.data;
                } else {
                    $('.mymsg').html(msg.message);
                    $('.mymsg').addClass('alert-warning');
                    $('.mymsg').removeClass('alert-success');
                }
                $('.loginlco').attr("disabled", false);
                $('.loginlco').html("Sign me in");
            },
            error: function(msg) {
                if (msg.responseJSON['status'] == 303) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 401) {
                    location.href = base_loc;
                }
                if (msg.responseJSON['status'] == 400) {
                    location.href = base_loc;
                }
            }
        });
    });
});