<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitepostajax extends CI_Controller {

	public function index()
	{
		$this->load->view('site/index');
	}

	public function login()
	{
		$this->load->view('site/login');
	}

	public function dashboard()
	{
		$this->load->view('web/header');
		$this->load->view('web/dashboard');
		$this->load->view('web/footer');
	}
}
