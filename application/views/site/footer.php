		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url();?>assets/themeassets/assets/js/app.min.js"></script>
	<script src="<?php echo base_url();?>assets/themeassets/assets/js/theme/google.min.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url();?>assets/themeassets/assets/plugins/d3/d3.min.js"></script>
	<script src="<?php echo base_url();?>assets/themeassets/assets/plugins/nvd3/build/nv.d3.min.js"></script>
	<script src="<?php echo base_url();?>assets/themeassets/assets/plugins/jvectormap-next/jquery-jvectormap.min.js"></script>
	<script src="<?php echo base_url();?>assets/themeassets/assets/plugins/jvectormap-next/jquery-jvectormap-world-mill.js"></script>
	<script src="<?php echo base_url();?>assets/themeassets/assets/plugins/apexcharts/dist/apexcharts.min.js"></script>
	<script src="<?php echo base_url();?>assets/themeassets/assets/plugins/moment/min/moment.min.js"></script>
	<script src="<?php echo base_url();?>assets/themeassets/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="<?php echo base_url();?>assets/themeassets/assets/js/demo/dashboard-v3.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','<?php echo base_url();?>assets/themeassets/google-analytics.com/analytics.js','ga');

		ga('create', 'UA-53034621-1', 'auto');
		ga('send', 'pageview');

	</script>
</body>
</html>